@module("SoluAdmin\\PermissionsCrud")
<!-- Users, Roles Permissions -->
<li class="treeview">
    <a href="#">
        <i class="fa fa-group"></i>
        <span>{{ trans('SoluAdmin::PermissionsCrud.menu-tree-parent') }}</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.PermissionsCrud.route_prefix', '') . '/user') }}">
                <i class="fa fa-user"></i> <span>{{ trans('SoluAdmin::PermissionsCrud.user_plural') }}</span>
            </a>
        </li>
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.PermissionsCrud.route_prefix', '') . '/role') }}">
                <i class="fa fa-group"></i> <span>{{ trans('SoluAdmin::PermissionsCrud.role_plural') }}</span>
            </a>
        </li>
        <li>
            <a href="{{ url(config('backpack.base.route_prefix', 'admin') . config('SoluAdmin.PermissionsCrud.route_prefix', '') . '/permission') }}">
                <i class="fa fa-key"></i> <span>{{ trans('SoluAdmin::PermissionsCrud.permission_plural') }}</span>
            </a>
        </li>
    </ul>
</li>
@endmodule