<?php

return [
    'route_prefix' => '',
    'middleware' => false,
    'setup_routes' => false,
    'publishes_migrations' => false,

    'user_model' => 'App\Models\Permission\User',

    'allow_permission_create' => true,
    'allow_permission_update' => true,
    'allow_permission_delete' => true,
    'allow_role_create' => true,
    'allow_role_update' => true,
    'allow_role_delete' => true,

    'connection' => 'authorization',
];
