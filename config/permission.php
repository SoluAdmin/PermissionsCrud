<?php

return [
    'models' => [
        'permission' => \SoluAdmin\PermissionsCrud\Models\Permission::class,
        'role' => \SoluAdmin\PermissionsCrud\Models\Role::class,
    ],

    'table_names' => [

        'roles' => 'roles',
        'permissions' => 'permissions',
        'model_has_permissions' => 'model_has_permissions',
        'model_has_roles' => 'model_has_roles',

        'role_has_permissions' => 'role_has_permissions',
    ],

    'cache_expiration_time' => 60 * 24,

    'log_registration_exception' => true,
];
