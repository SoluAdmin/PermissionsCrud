<?php

namespace SoluAdmin\PermissionsCrud\Models;

use Backpack\CRUD\CrudTrait;
use Spatie\Permission\Models\Permission as OriginalPermission;

class Permission extends OriginalPermission
{
    use CrudTrait;

    public $guarded = ['id'];

    protected $attribute = [
        'guard_name' => 'web'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection(config('SoluAdmin.PermissionsCrud.connection'));
    }
}
