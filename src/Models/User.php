<?php

namespace SoluAdmin\PermissionsCrud\Models;

use Backpack\Base\app\Notifications\ResetPasswordNotification;
use Backpack\CRUD\CrudTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use CrudTrait;
    use HasRoles;
    use Notifiable;

    protected $guarded = ['id'];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection(config('SoluAdmin.PermissionsCrud.connection'));
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function getAvatarImageAttribute()
    {
        $initial = mb_substr($this->name, 0, 1);

        return $this->avatar
            ? "/" . $this->avatar
            : "https://placehold.it/160x160/00a65a/ffffff/&text=$initial";
    }
}
