<?php

namespace SoluAdmin\PermissionsCrud\Providers;

use SoluAdmin\Support\Providers\CrudServiceProvider;
use SoluAdmin\Support\Helpers\PublishableAssets as Assets;
use Spatie\Permission\PermissionServiceProvider;

class PermissionsCrudServiceProvider extends CrudServiceProvider
{
    protected $assets = [
        Assets::CONFIGS,
        Assets::TRANSLATIONS,
        Assets::VIEWS,
        Assets::SEEDS,
    ];

    protected $resources = [
        'permission' => 'PermissionCrudController',
        'role' => 'RoleCrudController',
        'user' => 'UserCrudController',
    ];

    protected function registerExtras()
    {
        $this->app->register(PermissionServiceProvider::class);
    }
}
