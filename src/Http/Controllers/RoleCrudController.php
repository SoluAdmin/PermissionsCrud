<?php

namespace SoluAdmin\PermissionsCrud\Http\Controllers;

use SoluAdmin\PermissionsCrud\Http\Requests\RoleCrudRequest as StoreRequest;
use SoluAdmin\PermissionsCrud\Http\Requests\RoleCrudRequest as UpdateRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class RoleCrudController extends BaseCrudController
{
    public function model()
    {
        return config('permission.models.role');
    }

    public function setup()
    {
        parent::setup();

        if (config('SoluAdmin.PermissionsCrud.allow_role_create') == false) {
            $this->crud->denyAccess('create');
        }
        if (config('SoluAdmin.PermissionsCrud.allow_role_update') == false) {
            $this->crud->denyAccess('update');
        }
        if (config('SoluAdmin.PermissionsCrud.allow_role_delete') == false) {
            $this->crud->denyAccess('delete');
        }

        if (auth()->user()->hasRole('Root')) {
            $this->crud->addFields($this->form()->advancedFields());
        }
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
