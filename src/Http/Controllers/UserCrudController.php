<?php

namespace SoluAdmin\PermissionsCrud\Http\Controllers;

use Illuminate\Support\Facades\Request;
use SoluAdmin\PermissionsCrud\Http\Requests\UserStoreCrudRequest as StoreRequest;
use SoluAdmin\PermissionsCrud\Http\Requests\UserUpdateCrudRequest as UpdateRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class UserCrudController extends BaseCrudController
{
    public function model()
    {
        return config('SoluAdmin.PermissionsCrud.user_model');
    }

    public function setup()
    {
        parent::setup();
        $this->crud->enableAjaxTable();
    }

    public function store(StoreRequest $request)
    {
        $this->crud->hasAccessOrFail('create');

        if ($request->input('password')) {
            $item = $this->crud->create(Request::except(['redirect_after_save']));

            $item->password = bcrypt($request->input('password'));
            $item->save();
        } else {
            $item = $this->crud->create(Request::except(['redirect_after_save', 'password']));
        }

        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        $this->setSaveAction();

        return $this->performSaveAction($item->getKey());
    }

    public function update(UpdateRequest $request)
    {
        $this->crud->hasAccessOrFail('update');

        $dataToUpdate = Request::except(['redirect_after_save', 'password']);

        if ($request->input('password')) {
            $dataToUpdate['password'] = bcrypt($request->input('password'));
        }

        $this->crud->update(Request::get($this->crud->model->getKeyName()), $dataToUpdate);

        \Alert::success(trans('backpack::crud.update_success'))->flash();

        $this->setSaveAction();

        return $this->performSaveAction();
    }
}
