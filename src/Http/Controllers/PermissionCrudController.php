<?php

namespace SoluAdmin\PermissionsCrud\Http\Controllers;

use SoluAdmin\PermissionsCrud\Http\Requests\PermissionCrudRequest as StoreRequest;
use SoluAdmin\PermissionsCrud\Http\Requests\PermissionCrudRequest as UpdateRequest;
use SoluAdmin\Support\Http\Controllers\BaseCrudController;

class PermissionCrudController extends BaseCrudController
{

    public function model()
    {
        return config('permission.models.permission');
    }

    public function setup()
    {
        parent::setup();

        if (!config('SoluAdmin.PermissionsCrud.allow_permission_create')) {
            $this->crud->denyAccess('create');
        }
        if (!config('SoluAdmin.PermissionsCrud.allow_permission_update')) {
            $this->crud->denyAccess('update');
        }
        if (!config('SoluAdmin.PermissionsCrud.allow_permission_delete')) {
            $this->crud->denyAccess('delete');
        }

        if (auth()->user()->hasRole('Root')) {
            $this->crud->addFields($this->form()->advancedFields());
        }
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
