<?php

namespace SoluAdmin\PermissionsCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class UserCrudDataTable implements DataTable
{
    public function columns()
    {

        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::PermissionsCrud.name'),
                'type' => 'text',
            ],
            [
                'name' => 'email',
                'label' => trans('SoluAdmin::PermissionsCrud.email'),
                'type' => 'email',
            ],
            [
                'label' => trans('SoluAdmin::PermissionsCrud.role_plural'),
                'type' => 'select_multiple',
                'name' => 'roles',
                'entity' => 'roles',
                'attribute' => 'name',
                'model' => config('permission.models.role'),
                'morph' => true,
            ],
            [
                'label' => trans('SoluAdmin::PermissionsCrud.permission_plural'),
                'type' => 'select_multiple',
                'name' => 'permissions',
                'entity' => 'permissions',
                'attribute' => 'name',
                'model' => config('permission.models.permission'),
                'morph' => true,
            ],

        ];
    }
}
