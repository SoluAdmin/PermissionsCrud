<?php

namespace SoluAdmin\PermissionsCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class RoleCrudDataTable implements DataTable
{
    public function columns()
    {

        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::PermissionsCrud.name'),
                'type' => 'text',
            ],
            [
                'label' => trans('SoluAdmin::PermissionsCrud.permission_plural'),
                'type' => 'select_multiple',
                'name' => 'permissions',
                'entity' => 'permissions',
                'attribute' => 'name',
                'model' => config('laravel-permission.models.permission'),
                'pivot' => true,
            ],
        ];
    }
}
