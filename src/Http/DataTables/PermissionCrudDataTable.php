<?php

namespace SoluAdmin\PermissionsCrud\Http\DataTables;

use SoluAdmin\Support\Interfaces\DataTable;

class PermissionCrudDataTable implements DataTable
{
    public function columns()
    {

        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::PermissionsCrud.name'),
                'type' => 'text',
            ],
            [
                'label' => trans('SoluAdmin::PermissionsCrud.roles_have_permission'),
                'type' => 'select_multiple',
                'name' => 'roles',
                'entity' => 'roles',
                'attribute' => 'name',
                'model' => config('laravel-permission.models.role'),
                'pivot' => true,
            ],
        ];
    }
}
