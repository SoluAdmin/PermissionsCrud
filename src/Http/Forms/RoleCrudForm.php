<?php

namespace SoluAdmin\PermissionsCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class RoleCrudForm implements Form
{
    public function fields()
    {
        return [
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::PermissionsCrud.name'),
                'type' => 'text',
            ],
            [
                'label' => trans('SoluAdmin::PermissionsCrud.permission_plural'),
                'type' => 'checklist',
                'name' => 'permissions',
                'entity' => 'permissions',
                'attribute' => 'name',
                'model' => config('laravel-permission.models.permission'),
                'pivot' => true,
            ],
        ];
    }

    public function advancedFields()
    {
        return [
            [
                'name' => 'separator',
                'type' => 'custom_html',
                'value' => '<hr><h4>' . trans('SoluAdmin::PermissionsCrud.advanced') . '</h4><br>' .
                    '<p>' . trans('SoluAdmin::PermissionsCrud.advanced_warning') . '</p>',
            ],
            [
                'name' => 'guard_name',
                'label' => trans('SoluAdmin::PermissionsCrud.guard_name'),
                'type' => 'text',
            ],
        ];
    }
}
