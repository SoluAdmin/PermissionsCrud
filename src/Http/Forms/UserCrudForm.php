<?php

namespace SoluAdmin\PermissionsCrud\Http\Forms;

use SoluAdmin\Support\Interfaces\Form;

class UserCrudForm implements Form
{
    public function fields()
    {
        return [
            [
                'name' => 'avatar',
                'label' => trans('SoluAdmin::PermissionsCrud.avatar'),
                'type' => 'browse'
            ],
            [
                'name' => 'name',
                'label' => trans('SoluAdmin::PermissionsCrud.name'),
                'type' => 'text',
            ],
            [
                'name' => 'email',
                'label' => trans('SoluAdmin::PermissionsCrud.email'),
                'type' => 'email',
            ],
            [
                'name' => 'password',
                'label' => trans('SoluAdmin::PermissionsCrud.password'),
                'type' => 'password',
            ],
            [
                'name' => 'password_confirmation',
                'label' => trans('SoluAdmin::PermissionsCrud.password_confirmation'),
                'type' => 'password',
            ],
            [
                'label' => trans('SoluAdmin::PermissionsCrud.role_plural'),
                'type' => 'select2_multiple',
                'name' => 'roles',
                'entity' => 'roles',
                'attribute' => 'name',
                'model' => config('permission.models.role'),
                'morph' => true,
            ],
            [
                'label' => trans('SoluAdmin::PermissionsCrud.permission_extras'),
                'type' => 'select2_multiple',
                'name' => 'permissions',
                'entity' => 'permissions',
                'attribute' => 'name',
                'model' => config('permission.models.permission'),
                'morph' => true,
            ],
        ];
    }
}
