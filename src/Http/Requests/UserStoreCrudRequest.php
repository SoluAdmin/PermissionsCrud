<?php

namespace SoluAdmin\PermissionsCrud\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserStoreCrudRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
          'email'    => 'required|unique:users,email',
          'name'     => 'required',
          'password' => 'required|confirmed',
        ];
    }
}
