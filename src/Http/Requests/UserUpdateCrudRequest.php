<?php

namespace SoluAdmin\PermissionsCrud\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class UserUpdateCrudRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        $id = Route::current()->parameter('user');

        return [
            'email'    => 'required|unique:users,email,'.$id,
            'name'     => 'required',
            'password' => 'confirmed',
        ];
    }
}
